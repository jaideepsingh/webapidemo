using Microsoft.AspNetCore.Mvc;
using Xunit;
using WebApiDemo.Controllers;
using System.Collections.Generic;

namespace WebApiDemo.Tests
{
    public class ValuesControllerUnitTests
    {
        [Fact]
        public void TestGetValues()
        {
            var controller = new ValuesController();

            var result = controller.Get(6);

            //var expected = new List<string> { "1", "2", "3", "4", "5" };

            var expected = "6 is the actual value";
            Assert.NotEqual(expected, result); //Trying to see how pipelines work, ignore for now

        }
    }
}
