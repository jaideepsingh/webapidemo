FROM microsoft/dotnet:sdk AS build
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY WebApiDemo/*.csproj ./WebApiDemo/
COPY WebApiDemo.Tests/*.csproj ./WebApiDemo.Tests/
RUN dotnet restore

# Copy everything else and build
COPY WebApiDemo/. ./WebApiDemo/
WORKDIR /app/WebApiDemo 
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/WebApiDemo/out ./
ENTRYPOINT ["dotnet", "WebApiDemo.dll"]
