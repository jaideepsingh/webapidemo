To run locally first time do:
docker build -t webapidemo .
docker run -it --rm -p 5000:80 --name webapidemo webapidemo

For deploying to ECR:
docker tag webapidemo 947165770945.dkr.ecr.us-east-1.amazonaws.com/ecrsample
docker push 947165770945.dkr.ecr.us-east-1.amazonaws.com/ecrsample

TODO use bitbucket pipeline to automatically deploy latest docker image from ECR to ECS